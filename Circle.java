import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Circle"})
public class Circle extends HttpServlet {
protected void doGet(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {
response.setContentType("text/html;charset=UTF-8");
try (PrintWriter out = response.getWriter()) {
  
String raddi=request.getParameter("radius");
  
if(raddi!=null)
{
double area=3.14*(Double.parseDouble(raddi)*Double.parseDouble(raddi));
double circumference=2*3.14*(Double.parseDouble(raddi));
out.println("<h2>Area & Circumference of a Circle</h2>");
out.println("Area : "+area);
out.println("Perimeter : "+circumference);
  
}
else{
out.println("Empty Length or width");
}
  
}
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {
doGet(request, response);
}

}