<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<title>Login</title>
</head>
<body>
<div class="container">
<center><h2>Login form</h2></center>
<form class="form-horizontal" action="Login" method="POST">
<div class="form-group">
<label class="control-label col-sm-2" >Username:</label>
<div class="col-sm-10">
<input type="text" class="form-control" id="user" placeholder="Enter username" name="user">
</div>
</div>
<div class="form-group">
<label class="control-label col-sm-2" for="pwd">Password:</label>
<div class="col-sm-10">
<input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
</div>
</div>
<div class="form-group">

</div>
<div class="form-group">
<div class="col-sm-offset-2 col-sm-10">
<input type="submit" class="btn btn-success " value="Submit" >
  
<input type="reset" class="btn btn-primary " value="Reset" >
  
</div>
  
</div>
</form>
</div>
</body>
</html>