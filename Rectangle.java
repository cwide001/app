import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = {"/Rectangle"})
public class Rectangle extends HttpServlet {


protected void doGet(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {
response.setContentType("text/html;charset=UTF-8");
try (PrintWriter out = response.getWriter()) {
  
String length1=request.getParameter("length");
String width1=request.getParameter("width");
if(length1!=null && width1!=null)
{
double area=Double.parseDouble(length1)*Double.parseDouble(width1);
double perimeter=2*(Double.parseDouble(length1)*Double.parseDouble(width1));
out.println("<h2>Area & Perimeter of a rectangle</h2>");
out.println("Area : "+area);
out.println("Perimeter : "+perimeter);
  
}
else{
out.println("Empty Length or width");
}
}
}

@Override
protected void doPost(HttpServletRequest request, HttpServletResponse response)
throws ServletException, IOException {
doGet(request, response);
}

  
}